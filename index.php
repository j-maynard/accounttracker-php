<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- <base href="https://soc.uwl.ac.uk/~21213696/" /> -->
		<title>Account Tracker</title>
		<link rel="stylesheet" href="css/at.css" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta charset="utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!-- JavaScript Startup Script -->
		
		<!-- My JavaScript Object Libraries-->
		<script src="scripts/accounts.js" type="text/javascript"></script>
		<script src="scripts/transactions.js" type="text/javascript"></script>
		<!-- My JavaScript Function Libraries -->
		<script src="scripts/accounttracker.js" type="text/javascript"></script>
		
		<!-- Give the page a shortcut icon for modern browsers -->
		<!-- <link rel="shortcut icon" href="icons/main.ico" type="image/x-icon" /> -->
	</head>
	<body onload="startup()">
		
		<header>
			<h1><img src="images/money2.png" height="90" width="90" alt="[icon]"
			style="float: left;margin-right:20px;margin-top: -10px;">Account Tracker</h1>
			
			<div id="toptoolbar">
				<nav id="mainnav">&nbsp;
					<!-- <input type="button" value="Work Offline" name="offline"> -->
					<!-- <input type="button" value="Get Data from Server" name="saveserver" onclick="AT.getServerAccounts()"> -->
					<!-- <input type="button" value="Write Account to Server" name="saveserver" onclick="AT.writeAccountToServer()"> -->
					<!-- <input type="button" value="Download File" name="savelocal" onclick="AT.getDownload()"> -->
					<!-- <input type="button" value="Logout" name="logout"> -->
				</nav>
			</div>
		</header>



<div id="pagecontent">

<form action="#" id="accountForm" name="accountForm" method="post" onsubmit="return false">

<!-- Sample Account Data is: sortcode,accountno,accountname,startbal,od,odlimit: (: = next account) -->
<input type="hidden" id="csvSampleAccounts" value="20-33-41,2039932,Current Account,0.00,true,1000,test@test.com:30-11-32,09309343,Savings Account,1000,false,0,test@test.com">

<!-- Sample Transaction Data is one hidden item per account with a format of:
transDate, transComment, moneyin, moneyout: (: = Next Transaction) -->
<input type="hidden" id="csvSampleTransAC0" value="21/09/2013,September Pay,1200,0:01/10/2013,October Rent,0,400:03/10/2013,Electric Bill,0,100:03/10/2013,Water Bill,0,30:04/10/2013,Sainsbury Shopping,0,60.50:05/10/2013,Netflix,0,4.99:05/10/2013,Unblock-Us,0,2.99">
<input type="hidden" id="csvSampleTransAC1" value="01/07/2013,From Pay,100,0:01/08/2013,From Pay,80,0:01/09/2013,From Pay,20,0:01/10/2013,From Pay,50,0:01/11/2013,From Pay,100,0">

	<div id="accounts">
		<div id="actoolbar">
		</div>
		
		<div id="editAccount">
		</div>
		
		<div id="achelp">
		</div>
	</div>
	
	<div id="transactionarea">
		<div id="transtoolbar">
		</div>
		
		<div id="transactions">
		</div>
	</div>

</form>

	<footer>
		<nav id="botomnav">
		</nav>
	</footer>


</body>

</html>