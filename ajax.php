<?php

function getSavedAccounts($accounts) {
	//$accountsOutput = json_encode($accounts);
	//echo $accountsOutput;
	//echo var_dump($accounts);
	//$myAccount = count($accounts);
	if($accounts != 0){
		for($i = 0; $i < count($accounts); $i++) {
			//echo "<p>" + var_dump($accounts) + "</p><p></p>";
			
			if($jsonFile = fopen("./actfiles/" . $accounts[$i]['id'] . ".json", "r")) {
				$newAccounts[$i] = json_decode(fgets($jsonFile), true);
				//$newAccount[i] = "Round $i\n";
				
				fclose($jsonFile);
			}
		}
		//echo var_dump($newAccounts);
		//header('Content-Type: application/json;charset=utf-8');
		
		echo "{ \"account\":" . json_encode($newAccounts) . "}";
	}
	//No accounts CSV... lets check for orphoned JSON files.
	else
	{
		$filesfound = 0;
		$actFileList = scandir("./actfiles/");
		//echo var_dump($actFileList);
		for($i = 0; $i < count($actFileList); $i++) {
		
			if(strpos($actFileList[$i], ".json")) {
			
				if($jsonFile = fopen("./actfiles/" . $actFileList[$i], "r")) {
				
				$newAccounts[$filesfound] = json_decode(fgets($jsonFile), true);
			
				fclose($jsonFile);
				$filesfound++;
				}
				
			}
			
		}
		
		//No files found exit here...
		if($filesfound == 0) {
			echo "No Files Found";
			return 0;
		}
		// Otherwise recreate CSV...
		// Lets rewrite the Accounts CSV file... 
		if($file = fopen ("./actfiles/accounts.csv", "w")) {
				fputcsv($file,array(
				'this.accountid',
				'this.sortcode',
				'this.accountno',
				'this.accountname',
				'this.startbalance',
				'this.balance',
				'this.overdraft',
				'this.overlimit',
				'this.acemail'));

		for($i = 0; $i < count($newAccounts); $i++) {
					fputcsv($file, array(
						$newAccounts[$i]['accountid'],
						$newAccounts[$i]['sortcode'],
						$newAccounts[$i]['accountno'],
						$newAccounts[$i]['accountname'],
						$newAccounts[$i]['startbalance'],
						$newAccounts[$i]['balance'],
						$newAccounts[$i]['overdraft'],
						$newAccounts[$i]['overlimit'],
						$newAccounts[$i]['acemail']
						));
					
				}
		fclose($file);
		}
		
		//echo "Files found: $filesfound\n";
		//echo var_dump($newAccounts);
		
		if($filesfound > 0){
			// Return orphed JSONs
			echo "{ \"account\":" . json_encode($newAccounts) . "}";
		}
		else {
			echo "No Files Found";
		}
	}
	
}

function loadAccountsCSV() {
	if(file_exists("./actfiles/accounts.csv")) {
		if($file = fopen ("./actfiles/accounts.csv", "r")){
			//echo "File loaded";
		    $ln = 0;
		    
		    while ($line = fgetcsv($file)) {
		    	$actmp[$ln] = array(
		    		"id" => $line[0],
			    	"sortcode" => $line[1],
					"accountno" => $line[2],
					"accountname" => $line[3],
					"startbalance" => $line[4],
					"balance" => $line[5],
					"overdraft" => $line[6],
			    	"overlimit" => $line[7],
					"acemail" => $line[8]);
		    	
		    	//echo var_dump($actmp[$ln]);
		        $ln++;
		    }
		    
		    fclose ($file);
		    
		    if($ln >= 2) {
			    for($i = 1; $i < $ln; $i++) {
				    $accounts[$i-1] = $actmp[$i];
			    }
			    return $accounts;
			}
		    else {
			    return 0;
		    }
		    
		}
		
	}
	else
	{
		return 0;
	}
}

function saveFullAccount($AccountJSON) {

	$myaccount = json_decode($AccountJSON);

	unset($myaccount->balancelog);
	//echo var_dump($myaccount);
	
	$AccountJSON = json_encode($myaccount);
	//echo var_dump($AccountJSON);
	
	// Write the JSON file...
	if($file = fopen ("actfiles/" . $myaccount->accountid . ".json", "w")){
			fwrite($file, $AccountJSON);
			fclose($file);
			echo "File Written";
	}
	else {
		echo "There was a problem writing the file to disk";
	}
	
	//Update the Accounts CSV file...
	if(file_exists('./actfiles/accounts.csv')){
	
		// Test for duplicates
		$accounts = loadAccountsCSV();
		
		for($i = 0; $i < count($accounts); $i++){
			if($accounts[$i]['id'] == $myaccount->accountid)
			{
				$match = true;
				break;
			}
			else
				$match = false;
		}
		
		
		if(!$match && $file = fopen ("./actfiles/accounts.csv", "a")) {
			fputcsv($file, array(
			$myaccount->accountid,
			$myaccount->sortcode,
			$myaccount->accountno,
			$myaccount->accountname,
			$myaccount->startbalance,
			$myaccount->balance,
			$myaccount->overdraft,
			$myaccount->overlimit,
			$myaccount->acemail));
		}
		else {
			// Update the existing account in memory
			$accounts[$i]["id"] = $myaccount->accountid;
		    $accounts[$i]["sortcode"] = $myaccount->sortcode;
			$accounts[$i]["accountno"] = $myaccount->accountno;
			$accounts[$i]["accountname"] = $myaccount->accountname;
			$accounts[$i]["startbalance"] = $myaccount->startbalance;
			$accounts[$i]["balance"] = $myaccount->balance;
			$accounts[$i]["overdraft"] = $myaccount->overdraft;
		    $accounts[$i]["overlimit"] = $myaccount->overlimit;
			$accounts[$i]["acemail"] = $myaccount->acemail;
			
			// Rewrite file to disk...
			if($file = fopen ("./actfiles/accounts.csv", "w")) {
				fputcsv($file,array(
				'this.accountid',
				'this.sortcode',
				'this.accountno',
				'this.accountname',
				'this.startbalance',
				'this.balance',
				'this.overdraft',
				'this.overlimit',
				'this.acemail'));
			
				for($i = 0; $i < count($accounts); $i++) {
					fputcsv($file, $accounts[$i]);
					
				}
			}
		}
		
	}
	// If the file doesn't exist... create it
	else {
		if($file = fopen ("./actfiles/accounts.csv", "a")) {
			fputcsv($file,array(
			'AccountID',
			'SortCode',
			'AccountNo',
			'AccountName',
			'StartBalance',
			'CurrentBalance',
			'Overdraft',
			'OverdraftLimit',
			'AccountEMail'));
			
			fputcsv($file, array(
			$myaccount->accountid,
			$myaccount->sortcode,
			$myaccount->accountno,
			$myaccount->accountname,
			$myaccount->startbalance,
			$myaccount->balance,
			$myaccount->overdraft,
			$myaccount->overlimit,
			$myaccount->acemail));
		}
	}
	
	// Write the transactons to the CSV file if the account has any
	if(count($myaccount->transactions) > 0) {
		if($file = fopen ("./actfiles/". $myaccount->accountid . ".csv", "w")) {
			fputcsv($file,array(
			'AccountID',
			'TransactionDate',
			'Comment',
			'MoneyIn',
			'MoneyOut'));
			for($i = 0; $i < count($myaccount->transactions); $i++) {
				fputcsv($file,array(
					$myaccount->transactions[$i]->accountid,
					$myaccount->transactions[$i]->transdate,
					$myaccount->transactions[$i]->comment,
					$myaccount->transactions[$i]->moneyin,
					$myaccount->transactions[$i]->moneyout));
			}
		fclose($file);
	}
	else {
		if($file = fopen ("./actfiles/". $myaccount->accountid . ".csv", "w")) {
			fputcsv($file,array(
			'AccountID',
			'TransactionDate',
			'Comment',
			'MoneyIn',
			'MoneyOut'));
		fclose($file);
	}
	
}
}
}

function writeTransaction($AccountJSON) {
	//echo var_dump($AccountJSON);
	$myTransaction = json_decode($AccountJSON);
	//echo var_dump($myTransaction);
	
	// Load the account JSON 
	// First check the account files exist
	
	if(file_exists("./actfiles/" . $myTransaction->accountid . ".json") && file_exists("./actfiles/" . $myTransaction->accountid . ".csv"))
	{
		if($file = fopen ("./actfiles/". $myTransaction->accountid . ".json", "r")) {
			$myAccount = json_decode(fgets($file));
			fclose($file);
		}
		
		//echo var_dump($myAccount);
		
		//work out what to do with the transaction
		if($myTransaction->type == "out") {
			$moneyin = 0.00;
			
			//Test to make sure we have enough money
			if($myAccount->overdraft){
				$avbalance = $myAccount->overlimit + $myAccount->balance;
			}
			else {
				$avbalance = $myAccount->balance;				
			}
			
			if(($avbalance - $myTransaction->amount) < 0) {
				echo "No enough funds for transaction";
				return false;
			}
			else {
				$moneyout = $myTransaction->amount;
				$myAccount->balance = $myAccount->balance - $myTransaction->amount;
			}
		}
		else {
			$moneyin = $myTransaction->amount;
			$moneyout = 0.00;
			$myAccount->balance = $myAccount->balance + $myTransaction->amount;
		}
		
		//Turn the transaction in to an associative array
		$newTransaction = array(
			'accountid' => $myTransaction->accountid, 'transdate' => $myTransaction->date,
			'comment' => $myTransaction->comment, 'moneyin' => $moneyin, 'moneyout' => $moneyout);
		
		$myAccount->transactions[] = $newTransaction;
		
		if($file = fopen ("./actfiles/". $myTransaction->accountid . ".json", "w")) {
			fputs($file, json_encode($myAccount));
			fclose($file);
		}
		
		if($file = fopen ("./actfiles/". $myTransaction->accountid . ".csv", "a")) {
			fputcsv($file, $newTransaction);
			fclose($file);
		}
		updateAccountsCSV($myAccount);
		echo "All files on the server updated successfully.";
	}
	
}

function deleteTransActions($transactions) {
	
	$deletedTransactions = json_decode($transactions);
	
	//$delTransPeek = json_decode($transactions,true);
	//echo var_dump($delTransPeek);
	//return false;
	
	if(file_exists("./actfiles/" . $deletedTransactions[0]->accountid . ".json") &&
		file_exists("./actfiles/" . $deletedTransactions[0]->accountid . ".csv"))
	{
		if($file = fopen ("./actfiles/". $deletedTransactions[0]->accountid . ".json", "r")) {
			$myAccount = json_decode(fgets($file));
			fclose($file);
		}
		
	}
	
	for($i =0; $i < count($deletedTransactions); $i++) {
		//Update account balance...
		$myAccount->balance = $myAccount->balance + $deletedTransactions[$i]->moneyout - $deletedTransactions[$i]->moneyin;
		//Remove the transaction
		unset($myAccount->transactions[$deletedTransactions[$i]->index]);
	}
	
	//Reset the Arrays Index
	$myAccount->transactions = array_values($myAccount->transactions);
	
	//echo json_encode($myAccount);
	//return false;
	
	// Write the JSON file...
	if($file = fopen ("actfiles/" . $myAccount->accountid . ".json", "w")){
			fwrite($file, json_encode($myAccount));
			fclose($file);
			//echo "File Written";
	}
	else {
		echo "There was a problem writing the JSON file to disk";
		$errors = true;
	}
	
	//echo count($myAccount->transactions);
	
	// Write new account csv files...
	unlink("./actfiles/" . $myAccount->accountid . ".csv");
	
	if($file = fopen ("./actfiles/". $myAccount->accountid . ".csv", "w")) {
		fputcsv($file,array(
			'AccountID',
			'TransactionDate',
			'Comment',
			'MoneyIn',
			'MoneyOut'));
		for($i = 0; $i < count($myAccount->transactions); $i++) {
			fputcsv($file,array(
				$myAccount->transactions[$i]->accountid,
				$myAccount->transactions[$i]->transdate,
				$myAccount->transactions[$i]->comment,
				$myAccount->transactions[$i]->moneyin,
				$myAccount->transactions[$i]->moneyout));
		}
		fclose($file);
	}
	else {
		echo "There was a problem writing the CSV file to disk";
		$errors = true;
	}
	
	updateAccountsCSV($myAccount);

	echo "Transactions synced to server";
}

function updateAccountsCSV($myAccount) {
		//Update the accounts csv file...
		// Lets rewrite the Accounts CSV file...
		$accounts = loadAccountsCSV();
		
		if($file = fopen ("./actfiles/accounts.csv", "w")) {
				fputcsv($file,array(
				'this.accountid',
				'this.sortcode',
				'this.accountno',
				'this.accountname',
				'this.startbalance',
				'this.balance',
				'this.overdraft',
				'this.overlimit',
				'this.acemail'));

		for($i = 0; $i < count($accounts); $i++) {
		
					if($accounts[$i]['id'] == $myAccount->accountid)
					{
						$accounts[$i]['balance'] = $myAccount->balance;
					}
					
					fputcsv($file, array(
						$accounts[$i]['id'],
						$accounts[$i]['sortcode'],
						$accounts[$i]['accountno'],
						$accounts[$i]['accountname'],
						$accounts[$i]['startbalance'],
						$accounts[$i]['balance'],
						$accounts[$i]['overdraft'],
						$accounts[$i]['overlimit'],
						$accounts[$i]['acemail']
						));	
						
				}
				fclose($file);
				return true;
			}
	
	else {
		echo "There was a problem updating the accounts CSV file.";
		return false;
	}
}

function deleteAccount($accountID) {
		$accounts = loadAccountsCSV();
		
		if($file = fopen ("./actfiles/accounts.csv", "w")) {
				fputcsv($file,array(
				'this.accountid',
				'this.sortcode',
				'this.accountno',
				'this.accountname',
				'this.startbalance',
				'this.balance',
				'this.overdraft',
				'this.overlimit',
				'this.acemail'));
		for($i = 0; $i < count($accounts); $i++) {
		
					if($accounts[$i]['id'] == $accountID)
					{
						unlink("./actfiles/" . $accountID . ".csv");
						unlink("./actfiles/" . $accountID . ".json");
						continue; 
					}
					
					fputcsv($file, array(
						$accounts[$i]['id'],
						$accounts[$i]['sortcode'],
						$accounts[$i]['accountno'],
						$accounts[$i]['accountname'],
						$accounts[$i]['startbalance'],
						$accounts[$i]['balance'],
						$accounts[$i]['overdraft'],
						$accounts[$i]['overlimit'],
						$accounts[$i]['acemail']
						));	
						
				}
		fclose($file);
		
		//$olddir = getcwd(); // Save the current directory
		//chdir(getcwd() . "/actfiles/");
		
		@unlink(dirname($_SERVER["SCRIPT_FILENAME"]) . "/actfiles/" . $accountID . ".csv");
		@unlink(dirname($_SERVER["SCRIPT_FILENAME"]) . "/actfiles/" . $accountID . ".json");
		//chdir($olddir); // Restore the old working directory  
			
		//echo dirname($_SERVER["SCRIPT_FILENAME"]) . "/actfiles/";
		// /Users/jamie/Documents/University/Internet Technologies/Assignment 2/actfiles/
//  unlink(/Users/jamie/Documents/University/Internet Technologies/Assignment 2/actfiles/30113209309343.json)
		echo "Account Deleted from server...";
				
	}
}

function downloadTransCSV($accountID) {
	
	if(file_exists("./actfiles/" . $accountID . ".json") &&
		file_exists("./actfiles/" . $accountID . ".csv"))
	{
		if($file = fopen ("./actfiles/". $accountID . ".json", "r")) {
			$myAccount = json_decode(fgets($file));
			fclose($file);
		}
	}
	
	if($file = fopen ("./tmp/". $accountID . ".csv", "w")) {
			//mb_internal_encoding("ISO-8859-1");
			//fwrite($file, "\xEF\xBB\xBF");
			fputs($file, "Accounts Details:\n");
			
			fputcsv($file,array(
			'Sortcode',
			'Account Number',
			'Account Name',
			'Start Balance',
			'End Balance',
			'Overdraft',
			'Overdraft Limit'));
			
			if($myAccount->overdraft)
						{
							$od = "Yes";
						}
						else
						{
							$od = "No";
							$myAccount->overlimit = 0.00;
						}
						
			
			fputcsv($file, array(
						$myAccount->sortcode,
						$myAccount->accountno,
						$myAccount->accountname,
						$myAccount->startbalance,
						"£"  . number_format($myAccount->balance, 2),
						$od,
						"£"  . number_format($myAccount->overlimit,2)));
						
						
			fputs($file, "\n\nTransactions:\n");
			
			fputcsv($file,array(
			'TransactionDate',
			'Comment',
			'MoneyIn',
			'MoneyOut',
			'Balance',
			'Available Balance'));
			
			$currentBalance = $myAccount->startbalance;
			for($i = 0; $i < count($myAccount->transactions); $i++) {
			
				// Update the running balance
				$currentBalance = $currentBalance + $myAccount->transactions[$i]->moneyin - $myAccount->transactions[$i]->moneyout;
				if($myAccount->overdraft)
						{
							$avBalance = $currentBalance + $myAccount->overlimit;
						}
						else
						{
							$avBalance = $currentBalance;
						}
				
				fputcsv($file,array(
				$myAccount->transactions[$i]->transdate,
				$myAccount->transactions[$i]->comment,
				"£" . number_format($myAccount->transactions[$i]->moneyin, 2),
				"£" . number_format($myAccount->transactions[$i]->moneyout,2),
				"£"  . number_format($currentBalance, 2),
				"£" . number_format($avBalance, 2)));
			}
			
			
			fclose($file);
	}
	
	echo "./tmp/" . $myAccount->accountid . ".csv";
}

function unlinkCSVFile($filename) {

	if(file_exists($filename)) {
		unlink($filename);
		echo "CSV file deleted...";
	}
	else {
		echo "File does not exist!";
	}
	
}


$ATActions = $_POST["ATActions"];
$AccountJSON = $_POST["myData"];


if(get_magic_quotes_gpc()) { $AccountJSON = stripslashes($AccountJSON); }

//echo "<h1>Server function</H1><h2>$ACTActions</h2>";
//phpinfo();

switch($ATActions) {

	case "getACData":
		$myAccounts = loadAccountsCSV();
		getSavedAccounts($myAccounts);
		break;
		
	case "saveFullAccount":
		//echo "AccountJSON = " . var_dump($AccountJSON);
		saveFullAccount($AccountJSON);
		break;
	case "addTransAction":
		writeTransaction($AccountJSON);
		break;
	case "delTransactions":
		deleteTransActions($AccountJSON);
		break;
	case "delAccount":
		deleteAccount($AccountJSON);
		break;
	case "downloadCSV":
		downloadTransCSV($AccountJSON);
		break;
	case "unlinkFile":
		unlinkCSVFile($AccountJSON);
		break;
	default:
		echo "No action given ";
		//echo $ATActions;
		//echo " ";
		//echo $AccountJSON;
		//echo phpinfo();
		break;
}

//$myAccounts = loadAccountsCSV();
//writeSavedAccounts($myAccounts);
?>