function AccountTracker()
{
	this.actoolbarhook;
	this.accounthook;
	this.transtoolbarhook;
	this.transhook;
	this.opendate;
	this.acemail;
	this.help = true;
}

// Setting Methods
AccountTracker.prototype.setHelp = function(flag)
{
	this.help = flag;
	HelpH = document.getElementById('HelpH');
	if (!flag)
	{
		HelpH.innerHTML = "Help... Off";
	}
	else
	{
		HelpH.innerHTML = "Help...";
	}
}

AccountTracker.prototype.SetHooks = function(actoolbar, accountobj, transtoolbar, transobj)
{
	// Test to make sure these are DIV elements
	if (actoolbar.nodeName == "DIV" && accountobj.nodeName == "DIV" && transtoolbar.nodeName == "DIV" && transobj.nodeName == "DIV")
	{
		this.actoolbarhook = actoolbar;
		this.accounthook = accountobj;
		this.transtoolbarhook = transtoolbar;
		this.transhook = transobj;
		return true;
	}
	else
	{
		return false;
	}
}

// Get Methods
AccountTracker.prototype.getAcToolbar = function()
{
	return this.actoolbarhook;
}

AccountTracker.prototype.getAcArea = function()
{
	return this.accounthook;
}

AccountTracker.prototype.getTransToolbar = function()
{
	return this.transtoolbarhook;
}

AccountTracker.prototype.getTransArea = function()
{
	return this.transhook;
}

AccountTracker.prototype.loadACToolbar = function()
{
	// Create accounts tool bar buttons
	var acToolBarP = document.createElement("p");
	var newBtn = document.createElement("button");
	var accountList = document.createElement("select");
	var editBtn = document.createElement("button");
	var delBtn = document.createElement("button");
	var sampleBtn = document.createElement("button");
	acToolBarP.setAttribute("id", "ToolBarP");
	// Set up the new button
	newBtn.setAttribute("name", "newBtn");
	newBtn.setAttribute("id", "newBtn");
	newBtn.setAttribute("onclick", "AT.MakeForm(false)");
	newBtn.innerHTML = "New Account";
	// Set up the Account List select
	accountList.setAttribute("name", "accountList");
	accountList.setAttribute("id", "accountList");
	accountList.add(new Option("No Accounts", "x"));
	sampleBtn.setAttribute("name", "sampleBtn");
	sampleBtn.setAttribute("id", "sampleBtn");
	sampleBtn.setAttribute("onclick", "AT.loadSampleData()");
	sampleBtn.innerHTML = "Load Demo Data";
	// Attach Toolbar
	this.actoolbarhook.appendChild(acToolBarP);
	acToolBarP.appendChild(newBtn);
	acToolBarP.appendChild(accountList);
	acToolBarP.appendChild(sampleBtn);
}

AccountTracker.prototype.loadTransToolbar = function()
{
	if (document.getElementById("transToolBarP"))
	{
		return false;
	}
	// Create accounts tool bar buttons
	var transToolBarP = document.createElement("p");
	transToolBarP.setAttribute('id', 'transToolBarP')
	var newBtn = document.createElement("button");
	var accountList = document.createElement("select");
	var editBtn = document.createElement("button");
	var delBtn = document.createElement("button");
	var downloadBtn = document.createElement("button");
	// Set up the new button
	newBtn.setAttribute("name", "newTransBtn");
	newBtn.setAttribute("id", "newTransBtn");
	newBtn.setAttribute("onclick", "AT.newTrans()");
	newBtn.innerHTML = "New Transaction";
	delBtn.setAttribute("name", "delTransBtn");
	delBtn.setAttribute("id", "delTransBtn");
	delBtn.setAttribute("onclick", "AT.delSelectedTrans()");
	delBtn.setAttribute("disabled", "true");
	delBtn.innerHTML = "Delete Selected";
	
	downloadBtn.setAttribute("name", "downloadBtn");
	downloadBtn.setAttribute("id", "downloadBtn");
	downloadBtn.setAttribute("onclick", "AT.getDownload()");
	downloadBtn.innerHTML = "Download Account CSV";
	// Attach Toolbar
	this.transtoolbarhook.appendChild(transToolBarP);
	transToolBarP.appendChild(newBtn);
	transToolBarP.appendChild(delBtn);
	transToolBarP.appendChild(downloadBtn);
	//acToolBarP.appendChild(accountList);
	//Create transaction areas so we can enter new trans actions
	newTransHook = document.createElement('div');
	newTransHook.setAttribute('id', 'newTransHook');
	//Create a second area to display our transactions
	listTransHook = document.createElement('div');
	listTransHook.setAttribute('id', 'listTransHook');
	this.transhook.appendChild(newTransHook);
	this.transhook.appendChild(listTransHook);
}

AccountTracker.prototype.MakeForm = function(edit)
{
	//alert(document.getElementById('newAccountBtn').getAttribute('onclick'))
	// Make sure our form area is clear
	this.DestroyForm();
	if (edit)
	{
		//Get the index of the account we need
		i = document.getElementById("accountList").selectedIndex;
		//Get the account details we wish to edit
		var SortCode = myAccount[i].getSortCode();
		var acNo = myAccount[i].getAccountNo();
		var acName = myAccount[i].getAccountName();
		var acOD = myAccount[i].getOverdraft();
		var acLimit = myAccount[i].getOverlimit();
		var acBal = myAccount[i].getStartBalance();
		var email = myAccount[i].getAcEmail();
		var acHeading = document.createElement('h2');
		acHeading.setAttribute('id', 'acHeading');
		acHeading.innerHTML = "Edit Account"
	}
	else
	{
		var SortCode = "--";
		var acNo = "";
		var acName = "";
		var email = "";
		var edit = false;
		var acHeading = document.createElement('h2');
		acHeading.setAttribute('id', 'acHeading');
		acHeading.innerHTML = "Create Account"
	}
	var acP0 = document.createElement('p');
	acP0.setAttribute('id', 'acEmailP');
	var acEmailLabel = document.createElement('LABEL');
	acEmailLabel.setAttribute('for', 'Email');
	acEmailLabel.setAttribute('class', 'acLabel');
	acEmailLabel.innerHTML = "E-Mail:";
	var acEmailInput = document.createElement('input');
	acEmailInput.setAttribute('name', 'Email');
	acEmailInput.setAttribute('id', 'Email');
	acEmailInput.setAttribute('size', '20');
	acEmailInput.setAttribute('value', email);
	acEmailInput.setAttribute('onkeyup', "AT.acField('acEmailImg', " + edit + ")");
	acEmailInput.setAttribute('onFocus', "AT.acHelp('email','Email')");
	acEmailInput.setAttribute('onBlur', "AT.acClrHelp('Email')");
	var acP1 = document.createElement('p');
	acP1.setAttribute('id', 'acSortP')
	var acSortLabel = document.createElement('LABEL');
	acSortLabel.setAttribute('for', 'SortCode');
	acSortLabel.setAttribute('class', 'acLabel');
	acSortLabel.innerHTML = "Sort Code: ";
	codes = SortCode.split('-');
	var acCode1 = document.createElement('input');
	acCode1.setAttribute('name', 'code1');
	acCode1.setAttribute('id', 'code1');
	acCode1.setAttribute('size', '2');
	acCode1.setAttribute('maxlength', '2');
	acCode1.setAttribute('onkeyup', "AT.acField('sortCodeImg', " + edit + ")");
	acCode1.setAttribute('onFocus', "AT.acHelp('sortcode','code1')");
	acCode1.setAttribute('onBlur', "AT.acClrHelp('code1')");
	acCode1.setAttribute('value', codes[0]);
	var span1 = document.createElement('span');
	span1.innerHTML = " - ";
	var acCode2 = document.createElement('input');
	acCode2.setAttribute('name', 'code2');
	acCode2.setAttribute('id', 'code2');
	acCode2.setAttribute('size', '2');
	acCode2.setAttribute('maxlength', '2');
	acCode2.setAttribute('onkeyup', "AT.acField('sortCodeImg', " + edit + ")");
	acCode2.setAttribute('onFocus', "AT.acHelp('sortcode','code2')");
	acCode2.setAttribute('onBlur', "AT.acClrHelp('code2')");
	acCode2.setAttribute('value', codes[1]);
	var span2 = document.createElement('span');
	span2.innerHTML = " - ";
	var acCode3 = document.createElement('input');
	acCode3.setAttribute('name', 'code3');
	acCode3.setAttribute('id', 'code3');
	acCode3.setAttribute('size', '2');
	acCode3.setAttribute('maxlength', '2');
	acCode3.setAttribute('onkeyup', "AT.acField('sortCodeImg', " + edit + ")");
	acCode3.setAttribute('onFocus', "AT.acHelp('sortcode','code3')");
	acCode3.setAttribute('onBlur', "AT.acClrHelp('code3')");
	acCode3.setAttribute('value', codes[2]);
	var acP2 = document.createElement('p');
	acP2.setAttribute('id', 'AcNoP');
	var acNoLabel = document.createElement('LABEL');
	acNoLabel.setAttribute('for', 'AccountNumber');
	acNoLabel.setAttribute('class', 'acLabel');
	acNoLabel.innerHTML = "Account No.: ";
	var acNoInput = document.createElement('input');
	acNoInput.setAttribute('name', 'AccountNumber');
	acNoInput.setAttribute('id', 'AccountNumber');
	acNoInput.setAttribute('size', '10');
	acNoInput.setAttribute('maxlength', '10');
	acNoInput.setAttribute('onkeyup', "AT.acField('acNoImg', " + edit + ")");
	acNoInput.setAttribute('onFocus', "AT.acHelp('acno','AccountNumber')");
	acNoInput.setAttribute('onBlur', "AT.acClrHelp('AccountNumber')");
	acNoInput.setAttribute('value', acNo);
	var acP3 = document.createElement('p');
	acP3.setAttribute('id', 'acNameP');
	var acNameLabel = document.createElement('LABEL');
	acNameLabel.setAttribute('for', 'AccountName');
	acNameLabel.setAttribute('class', 'acLabel');
	acNameLabel.innerHTML = "Account Name: ";
	var acNameInput = document.createElement('input');
	acNameInput.setAttribute('name', 'AccountName');
	acNameInput.setAttribute('id', 'AccountName');
	acNameInput.setAttribute('size', '20');
	acNameInput.setAttribute('onkeyup', "AT.acField('acNameImg', " + edit + ")");
	acNameInput.setAttribute('onFocus', "AT.acHelp('acname','AccountName')");
	acNameInput.setAttribute('onBlur', "AT.acClrHelp('AccountName')");
	acNameInput.setAttribute('value', acName);
	if (edit)
	{
		var acP4 = document.createElement('p');
		acP4.setAttribute('id', 'acODP');
		var acODLabel = document.createElement('LABEL');
		acODLabel.setAttribute('for', 'acOD');
		acODLabel.setAttribute('class', 'acLabel');
		acODLabel.innerHTML = "Over Draft: ";
		var acODCheck = document.createElement('input');
		acODCheck.setAttribute('name', 'acOD');
		acODCheck.setAttribute('id', 'acOD');
		acODCheck.setAttribute('type', 'checkbox');
		acODCheck.setAttribute('value', "true");
		acODCheck.setAttribute('onclick', "var obj = document.getElementById('acODLimit'); var acODP = document.getElementById('acODP'); if(this.checked) \
			{ obj.disabled=false; AT.reqIcon(acODP, 'odLimit');} else {obj.disabled=true; obj.value='0.00'; acODP.removeChild(document.getElementById('odLimitImg')); }");
		if (acOD)
		{
			acODCheck.setAttribute("checked", "checked");
		}
		var acODLimitLabel = document.createElement('LABEL');
		acODLimitLabel.setAttribute('for', 'acODLimit');
		//acODLimitLabel.setAttribute('class', 'acLabel');
		acODLimitLabel.innerHTML = "Limit: £";
		var acODLimit = document.createElement('input');
		acODLimit.setAttribute('name', 'acODLimit');
		acODLimit.setAttribute('id', 'acODLimit');
		acODLimit.setAttribute('size', '10');
		acODLimit.setAttribute('onkeyup', "AT.acField('odLimitImg', " + edit + ")");
		acODLimit.setAttribute('onFocus', "AT.acHelp('money','acODLimit')");
		acODLimit.setAttribute('onBlur', "AT.acClrHelp('acODLimit')");
		acODLimit.setAttribute('value', acLimit);
		if (!acOD)
		{
			acODLimit.setAttribute('disabled', true);
		}
		var acP6 = document.createElement('p');
		acP6.setAttribute('id', 'acBalP');
		var acBalLabel = document.createElement('LABEL');
		acBalLabel.setAttribute('for', 'acBalance');
		acBalLabel.setAttribute('class', 'acLabel');
		acBalLabel.innerHTML = "Start Balance: ";
		var acBalance = document.createElement('input');
		acBalance.setAttribute('name', 'acBalance');
		acBalance.setAttribute('id', 'acBalance');
		acBalance.setAttribute('size', '10');
		acBalance.setAttribute('onkeyup', 'AT.acField("acBalImg", ' + edit + ')')
		acBalance.setAttribute('onfocus', 'if(!document.getElementById("acBalImg")) {AT.reqIcon(document.getElementById("acBalP"), "acBal");} AT.acHelp("money","acBalance")');
		acBalance.setAttribute('onblur', 'if(document.getElementById("acBalImg").src != "images/cross.png") { document.getElementById("acBalP").removeChild(document.getElementById("acBalImg"));} AT.acClrHelp("acBalance")');
		acBalance.setAttribute('value', acBal);
		var acP7 = document.createElement('p');
		acP7.setAttribute('id', 'acButtonP');
		var editBtn = document.createElement('button');
		editBtn.setAttribute("name", "editBtn");
		editBtn.setAttribute("id", "editBtn");
		editBtn.setAttribute("onclick", "AT.editBtn(" + i + ")");
		editBtn.innerHTML = "Update Account";
		var cancelBtn = document.createElement('button');
		cancelBtn.setAttribute("name", "cancelBtn");
		cancelBtn.setAttribute("id", "cancelBtn");
		cancelBtn.setAttribute("onclick", "AT.cancelBtn(" + i + ")");
		cancelBtn.innerHTML = "Cancel";
	}
	else
	{
		var acP4 = document.createElement('p');
		acP4.setAttribute('id', 'acButtonP');
		var addBtn = document.createElement('button');
		addBtn.setAttribute("name", "addBtn");
		addBtn.setAttribute("id", "addBtn");
		addBtn.setAttribute("onclick", "AT.addBtn()");
		addBtn.setAttribute('disabled', 'true');
		addBtn.innerHTML = "Add Account";
		var resetBtn = document.createElement('button');
		resetBtn.setAttribute("name", "cancelBtn");
		resetBtn.setAttribute("id", "cancelBtn");
		resetBtn.setAttribute("onclick", "AT.cancelBtn(0)");
		resetBtn.innerHTML = "Cancel";
	}
	var helpSpan = document.createElement('span');
	helpSpan.setAttribute('id', 'acHelpSpan')
	helpSpan.innerHTML = "Help: ";
	var helpOn = document.createElement('input');
	helpOn.setAttribute('type', 'radio');
	helpOn.setAttribute('id', 'acHelpOn');
	helpOn.setAttribute('name', 'acHelp');
	helpOn.setAttribute('onclick', 'AT.setHelp(true)')
	if (this.help)
	{
		helpOn.setAttribute('checked', 'checked');
	}
	helpOn.setAttribute('value', 'on');
	var helpOnL = document.createElement('label');
	helpOnL.setAttribute('id', 'helpOnL');
	helpOnL.setAttribute('for', 'acHelpOn');
	helpOnL.innerHTML = " On ";
	var helpOff = document.createElement('input');
	helpOff.setAttribute('type', 'radio');
	helpOff.setAttribute('id', 'acHelpOff');
	helpOff.setAttribute('name', 'acHelp');
	helpOff.setAttribute('onclick', 'AT.setHelp(false)')
	if (!this.help)
	{
		helpOff.setAttribute('checked', 'checked');
	}
	helpOff.setAttribute('value', 'off');
	var helpOffL = document.createElement('label');
	helpOffL.setAttribute('id', 'helpOffL');
	helpOffL.setAttribute('for', 'acHelpOff');
	helpOffL.innerHTML = " Off ";
	// Generate Form on Document
	this.accounthook.appendChild(acHeading);
	this.accounthook.appendChild(acP0);
	acP0.appendChild(acEmailLabel);
	acP0.appendChild(acEmailInput);
	this.reqIcon(acP0, 'acEmail');
	this.accounthook.appendChild(acP1);
	acP1.appendChild(acSortLabel);
	acP1.appendChild(acCode1);
	acP1.appendChild(span1);
	acP1.appendChild(acCode2);
	acP1.appendChild(span2);
	acP1.appendChild(acCode3);
	this.reqIcon(acP1, 'sortCode');
	this.accounthook.appendChild(acP2);
	acP2.appendChild(acNoLabel);
	acP2.appendChild(acNoInput);
	this.reqIcon(acP2, 'acNo');
	this.accounthook.appendChild(acP3);
	acP3.appendChild(acNameLabel);
	acP3.appendChild(acNameInput);
	this.reqIcon(acP3, 'acName');
	if (edit)
	{
		this.accounthook.appendChild(acP4);
		acP4.appendChild(acODLabel);
		acP4.appendChild(acODCheck);
		acP4.appendChild(acODLimitLabel);
		acP4.appendChild(acODLimit);
		if (acOD)
		{
			this.reqIcon(acP4, 'odLimit');
		}
		this.accounthook.appendChild(acP6);
		acP6.appendChild(acBalLabel);
		acP6.appendChild(acBalance);
		this.accounthook.appendChild(acP7);
		acP7.appendChild(editBtn);
		acP7.appendChild(cancelBtn);
		acP7.appendChild(helpSpan);
		acP7.appendChild(helpOn);
		acP7.appendChild(helpOnL);
		acP7.appendChild(helpOff);
		acP7.appendChild(helpOffL);
	}
	else
	{
		this.accounthook.appendChild(acP4);
		acP4.appendChild(addBtn);
		acP4.appendChild(resetBtn);
		acP4.appendChild(helpSpan);
		acP4.appendChild(helpOn);
		acP4.appendChild(helpOnL);
		acP4.appendChild(helpOff);
		acP4.appendChild(helpOffL);
	}
}

AccountTracker.prototype.DestroyForm = function()
{
	//Destroy form
	while (this.accounthook.firstChild)
	{
		this.accounthook.removeChild(this.accounthook.firstChild);
	}
}

AccountTracker.prototype.DisplayAccount = function(i)
{
	//Destroy whats in the form area
	this.DestroyForm();
	//alert("Display Account value of i: " + i);
	var SortCode = myAccount[i].getSortCode();
	var acNo = myAccount[i].getAccountNo();
	var acName = myAccount[i].getAccountName();
	var acOD = myAccount[i].getOverdraft();
	var acLimit = myAccount[i].getOverlimit();
	var acBal = myAccount[i].getBalance();
	var acEmail = myAccount[i].getAcEmail();
	var acODText;
	if (acOD)
	{
		acODText = '<input type="checkbox" name="od" id="od" checked="checked" disabled="false"> Limit: £' + acLimit;
	}
	else
	{
		acODText = '<input type="checkbox" name="od" id="od" disabled="false"> Limit: N/A';
	}
	var acHeading = document.createElement('h2');
	acHeading.setAttribute('id', 'acHeading');
	acHeading.innerHTML = "Account Details"
	var acPEmail = document.createElement('p');
	var acEmailL = document.createElement('span');
	acEmailL.setAttribute('id', 'acEmailL');
	acEmailL.setAttribute('class', 'acLabel');
	acEmailL.innerHTML = 'Account E-Mail: ';
	var acEmailF = document.createElement('span');
	acEmailF.setAttribute('id', 'acEmailF');
	acEmailF.innerHTML = acEmail;
	var acPName = document.createElement('p');
	var acNameL = document.createElement('span');
	acNameL.setAttribute('id', 'acNameL');
	acNameL.setAttribute('class', 'acLabel');
	acNameL.innerHTML = 'Account Name: ';
	var acNameF = document.createElement('span');
	acNameF.setAttribute('id', 'acNameF');
	acNameF.innerHTML = acName;
	var acPAcNo = document.createElement('p');
	var acNoL = document.createElement('span');
	acNoL.setAttribute('id', 'acNoL');
	acNoL.setAttribute('class', 'acLabel');
	acNoL.innerHTML = 'Account: ';
	var acNoF = document.createElement('span');
	acNoF.setAttribute('id', 'acNoF');
	acNoF.innerHTML = SortCode + " " + acNo;
	var acPOD = document.createElement('p');
	var acODL = document.createElement('span');
	acODL.setAttribute('id', 'acODL');
	acODL.setAttribute('class', 'acLabel');
	acODL.innerHTML = 'Overdraft: ';
	var acODF = document.createElement('span');
	acODF.setAttribute('id', 'acODF');
	acODF.innerHTML = acODText;
	var acPCBal = document.createElement('p');
	var acCBalL = document.createElement('span');
	acCBalL.setAttribute('id', 'acCBalL');
	acCBalL.setAttribute('class', 'acLabel');
	acCBalL.innerHTML = 'Current Balance: ';
	var acCBalF = document.createElement('span');
	acCBalF.setAttribute('id', 'acCBalF');
	acCBalF.innerHTML = " £ " + Number(acBal).toFixed(2);
	var acPABal = document.createElement('p');
	var acABalL = document.createElement('span');
	acABalL.setAttribute('id', 'acABalL');
	acABalL.setAttribute('class', 'acLabel');
	acABalL.innerHTML = 'Current Balance: ';
	var acABalF = document.createElement('span');
	acABalF.setAttribute('id', 'acABalF');
	acABalF.innerHTML = " £ " + String(Number(Number(acBal) + Number(acLimit)).toFixed(2));
	var acButtonP = document.createElement('p');
	var acEditBtn = document.createElement('button');
	acEditBtn.setAttribute("name", "editBtn");
	acEditBtn.setAttribute("id", "editBtn");
	acEditBtn.setAttribute("onclick", "AT.MakeForm(true)");
	acEditBtn.innerHTML = "Edit Account";
	
	var acDelBtn = document.createElement('button');
	acDelBtn.setAttribute("name", "acDelBtn");
	acDelBtn.setAttribute("id", "acDelBtn");
	acDelBtn.setAttribute("onclick", "AT.delAccount()");
	acDelBtn.innerHTML = "Delete Account";
	
	this.accounthook.appendChild(acHeading);
	this.accounthook.appendChild(acPEmail);
	acPEmail.appendChild(acEmailL);
	acPEmail.appendChild(acEmailF);
	this.accounthook.appendChild(acPName);
	acPName.appendChild(acNameL);
	acPName.appendChild(acNameF);
	this.accounthook.appendChild(acPAcNo);
	acPAcNo.appendChild(acNoL);
	acPAcNo.appendChild(acNoF);
	this.accounthook.appendChild(acPOD);
	acPOD.appendChild(acODL);
	acPOD.appendChild(acODF);
	this.accounthook.appendChild(acPCBal);
	acPCBal.appendChild(acCBalL);
	acPCBal.appendChild(acCBalF);
	this.accounthook.appendChild(acPABal);
	acPABal.appendChild(acABalL);
	acPABal.appendChild(acABalF);
	this.accounthook.appendChild(acButtonP);
	acButtonP.appendChild(acEditBtn);
	acButtonP.appendChild(acDelBtn);
	acLabels = document.getElementsByClassName('acLabel');
	for (i = 0; i < acLabels.length; i++)
	{
		acLabels[i].setAttribute('style', 'display: inline-block; width: 130px; font-weight: bold;');
	}
}

// Event Handlers
AccountTracker.prototype.cancelBtn = function(i)
{
	accountList = document.getElementById("accountList");
	if (accountList.options[0].value != "x")
	{
		this.DisplayAccount(i);
	}
	else
	{
		this.DestroyForm();
	}
}

AccountTracker.prototype.addBtn = function()
{
	var i = myAccount.length;
	//Set up a new account
	myAccount[i] = new Account()
	//get our form elemetns
	sort1 = document.getElementById("code1").value;
	sort2 = document.getElementById("code2").value;
	sort3 = document.getElementById("code3").value;
	acno = document.getElementById("AccountNumber").value;
	acname = document.getElementById("AccountName").value;
	acemail = document.getElementById("Email").value;
	//On click add the account
	if (myAccount[i].createAccount(sort1, sort2, sort3, acno, acname, acemail))
	{
		if (!document.getElementById('transToolBarP'))
		{
			this.loadTransToolbar();
		}
	}
	
	//Add the account to our account list
	accountList = document.getElementById("accountList");
	accountList.add(new Option(myAccount[i].getAccountName() + 
		" (" + myAccount[i].getSortCode() + " " + 
		myAccount[i].getAccountNo() + ")", i));
	accountList.selectedIndex = i;
	if (accountList.options[0].value == "x")
	{
		accountList.setAttribute('onchange', 'AT.changeAccount()')
		accountList.remove(0);
	}
	
	// Write the account to the server
	this.callAjax("./ajax.php", "saveFullAccount", JSON.stringify(myAccount[i]), this.accountWritten);
	
	// Display account
	this.DisplayAccount(i);
	this.displayTransactions(i);
}

AccountTracker.prototype.editBtn = function(i)
{
	//Grab data from the form
	sort1 = document.getElementById("code1").value;
	sort2 = document.getElementById("code2").value;
	sort3 = document.getElementById("code3").value;
	acno = document.getElementById("AccountNumber").value;
	acname = document.getElementById("AccountName").value;
	acod = document.getElementById("acOD").checked;
	odlimit = Number(document.getElementById("acODLimit").value);
	bal = Number(document.getElementById("acBalance").value);
	acemail = document.getElementById("Email").value
	if (!acod)
	{
		odlimit = 0.0;
	}
	//Edit the account
	myAccount[i].editAccount(sort1, sort2, sort3, acno, acname, bal, acod, odlimit, acemail);
	//Destroy the Form
	
	// Write the account to the server
	this.callAjax("./ajax.php", "saveFullAccount", JSON.stringify(myAccount[i]), this.accountWritten);
	
	this.DisplayAccount(i);
	this.displayTransactions(i);
}

AccountTracker.prototype.changeAccount = function()
{
	accountList = document.getElementById("accountList");
	this.DisplayAccount(accountList.selectedIndex);
	this.displayTransactions(accountList.selectedIndex);
}

AccountTracker.prototype.newTrans = function()
{
	// Disable New Transaction button to stop repeated clicks
	document.getElementById('newTransBtn').disabled = true;
	newTransArea = document.getElementById('newTransHook');
	var newTransHeading = document.createElement('H2');
	newTransHeading.setAttribute('id', 'newTransHeading');
	newTransHeading.innerHTML = "New Transaction";
	var newTransTbl = document.createElement('TABLE');
	newTransTbl.setAttribute('id', 'newTransTable');
	var newR1 = document.createElement('THEAD');
	newR1.setAttribute('id', 'newR1');
	var newR1C1 = document.createElement('TH');
	newR1C1.setAttribute('id', 'newR1C1');
	newR1C1.innerHTML = "Date";
	var newR1C2 = document.createElement('TH');
	newR1C2.setAttribute('id', 'newR1C2');
	newR1C2.innerHTML = "Comment";
	var newR1C3 = document.createElement('TH');
	newR1C3.setAttribute('id', 'newR1C3');
	newR1C3.innerHTML = "Type";
	var newR1C4 = document.createElement('TH');
	newR1C4.setAttribute('id', 'newR1C4');
	newR1C4.innerHTML = "Amount";
	var newR2 = document.createElement('TR');
	newR2.setAttribute('id', 'newR2');
	var newR2C1 = document.createElement('TD');
	newR2C1.setAttribute('id', 'newR2C1');
	var newR2C2 = document.createElement('TD');
	newR2C2.setAttribute('id', 'newR2C2');
	var newR2C3 = document.createElement('TD');
	newR2C3.setAttribute('id', 'newR2C3');
	var newR2C4 = document.createElement('TD');
	newR2C4.setAttribute('id', 'newR2C4');
	var transDate = document.createElement('input');
	transDate.setAttribute('name', 'transDate');
	transDate.setAttribute('type', 'text');
	transDate.setAttribute('id', 'transDate');
	transDate.setAttribute('size', '10');
	transDate.setAttribute('onkeyup', "AT.transField('transDateImg')");
	transDate.setAttribute('onfocus', "AT.acHelp('date','transDate')");
	transDate.setAttribute('onblur', "AT.acClrHelp('transDate')");
	transDate.setAttribute('value', 'dd/mm/yyyy');
	var transComment = document.createElement('input');
	transComment.setAttribute('name', 'transComment');
	transComment.setAttribute('type', 'text');
	transComment.setAttribute('id', 'transComment');
	transComment.setAttribute('size', '20');
	transComment.setAttribute('value', 'Comment');
	transComment.setAttribute('onkeyup', "AT.transField('transCommentImg')");
	transComment.setAttribute('onfocus', "AT.acHelp('comment','transComment')");
	transComment.setAttribute('onblur', "AT.acClrHelp('transComment')");
	var transType1 = document.createElement('input');
	transType1.setAttribute('name', 'transType');
	transType1.setAttribute('type', 'radio')
	transType1.setAttribute('id', 'transType1');
	transType1.setAttribute('value', 'in');
	transType1.setAttribute('onclick', "AT.transField('transTypeImg')");
	var transType1L = document.createElement('label');
	transType1L.setAttribute('for', 'transType1');
	transType1L.setAttribute('id', 'transType1L');
	transType1L.innerHTML = "Money In";
	var transType2 = document.createElement('input');
	transType2.setAttribute('name', 'transType');
	transType2.setAttribute('type', 'radio')
	transType2.setAttribute('id', 'transType2');
	transType2.setAttribute('value', 'out');
	transType2.setAttribute('onclick', "AT.transField('transTypeImg')");
	var transType2L = document.createElement('label');
	transType2L.setAttribute('for', 'transType2');
	transType2L.setAttribute('id', 'transType2L');
	transType2L.innerHTML = "Money Out";
	var transAmount = document.createElement('input');
	transAmount.setAttribute('name', 'transAmount');
	transAmount.setAttribute('type', 'text');
	transAmount.setAttribute('id', 'transAmount');
	transAmount.setAttribute('size', '10');
	transAmount.setAttribute('value', '0.00');
	transAmount.setAttribute('onkeyup', "AT.transField('transAmountImg')");
	transAmount.setAttribute('onfocus', "AT.acHelp('money','transAmount')");
	transAmount.setAttribute('onblur', "AT.acClrHelp('transAmount')");
	var newTransP = document.createElement('P');
	newTransP.setAttribute('id', 'newTransP');
	var newTransAddBtn = document.createElement('button');
	newTransAddBtn.setAttribute('id', 'newTransAddBtn');
	newTransAddBtn.setAttribute('onclick', 'AT.newTransAddBtn()');
	newTransAddBtn.setAttribute('disabled', 'true');
	newTransAddBtn.innerHTML = "Add Transaction";
	var newTransCancelBtn = document.createElement('button');
	newTransCancelBtn.setAttribute('id', 'newTransCancelBtn');
	newTransCancelBtn.setAttribute('onclick', 'AT.newTransCancelBtn()');
	newTransCancelBtn.innerHTML = "Cancel";
	newTransArea.appendChild(newTransHeading);
	newTransArea.appendChild(newTransTbl);
	newTransTbl.appendChild(newR1);
	newR1.appendChild(newR1C1);
	this.reqIcon(newR1C1, 'transDate');
	newR1.appendChild(newR1C2);
	this.reqIcon(newR1C2, 'transComment');
	newR1.appendChild(newR1C3);
	this.reqIcon(newR1C3, 'transType');
	newR1.appendChild(newR1C4);
	this.reqIcon(newR1C4, 'transAmount');
	newTransTbl.appendChild(newR2);
	newR2.appendChild(newR2C1);
	newR2C1.appendChild(transDate);
	newR2.appendChild(newR2C2);
	newR2C2.appendChild(transComment);
	newR2.appendChild(newR2C3);
	newR2C3.appendChild(transType1);
	newR2C3.appendChild(transType1L);
	newR2C3.appendChild(transType2);
	newR2C3.appendChild(transType2L);
	newR2.appendChild(newR2C4);
	newR2C4.appendChild(transAmount);
	newTransArea.appendChild(newTransP);
	newTransP.appendChild(newTransAddBtn);
	newTransP.appendChild(newTransCancelBtn);
}

AccountTracker.prototype.transValid = function()
{
	transDate = document.getElementById('transDate').value;
	transComment = document.getElementById('transComment').value;
	transAmount = document.getElementById('transAmount').value;
	transType = document.getElementsByName('transType');
	if (transType[0].checked || transType[1].checked)
	{
		transChecked = true
	}
	else
	{
		transChecked = false;
	}
	newDate = transDate.split('/');
	testDate = new Date(newDate[2], newDate[1] - 1, newDate[0]);
	if (testDate != "Invalid Date" && !isNaN(transAmount) && transChecked)
	{
		fail = false;
	}
	else
	{
		fail = true;
	}
	if (fail)
	{
		document.getElementById('newTransAddBtn').disabled = true;
		return false;
	}
	else
	{
		reqIcons = document.getElementsByClassName('reqIcon');
		for (i = 0; i < reqIcons.length; i++)
		{
			reqIcons[i].setAttribute('src', 'images/tick.png');
		}
		document.getElementById('newTransAddBtn').disabled = false;
		return true;
	}
}

AccountTracker.prototype.transField = function(hookimg)
{
	myImg = document.getElementById(hookimg);
	//First test the form
	if (!this.transValid())
	{
		myImg.setAttribute('src', 'images/cross.png');
	}
	//Full form validated no point carrying on
	else
	{
		return true;
	}
	if (hookimg == 'transDateImg')
	{
		transDate = document.getElementById('transDate').value;
		newDate = transDate.split('/');
		testDate = new Date(newDate[2], newDate[1] - 1, newDate[0]);
		if (testDate == "Invalid Date")
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
	}
	if (hookimg == 'transCommentImg')
	{
		var commentre = /(\<.+?\>)/; //Test for HTML Tags
		transComment = document.getElementById('transComment').value;
		if (commentre.test(transComment))
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
	}
	if (hookimg == 'transAmountImg')
	{
		transAmount = document.getElementById('transAmount').value;
		if (isNaN(transAmount))
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
	}
	if (hookimg == 'transTypeImg')
	{
		transType = document.getElementsByName('transType');
		if (transType[0].checked || transType[1].checked)
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
	}
}

AccountTracker.prototype.newTransAddBtn = function()
{
	acindex = document.getElementById('accountList').selectedIndex;
	transComment = document.getElementById('transComment').value;
	transTypeRadio = document.getElementsByName('transType');
	transAmount = Number(document.getElementById('transAmount').value);

	var transType;
	var moneyin = Number();
	var moneyout = Number();
	
	//Convert date to the right format for a date object
	transDateString = document.getElementById('transDate').value.split('/');
	var day = Number(transDateString[0]);
	var month = Number(transDateString[1]);
	var year = Number(transDateString[2]);
	var transDate = month + "/" + day + "/" + year
	if (transTypeRadio[0].checked)
	{
		transType = "in";
		moneyin = transAmount;
		moneyout = 0.0;
	}
	else if (transTypeRadio[1].checked)
	{
		transType = "out"
		moneyin = 0.0
		moneyout = transAmount;
	}
	else
	{
		alert("Please select a transaction type before continuing")
		return false;
	}
	
	//Check to make sure money out doesn't exceed our available balance
	if(transType == "out")
	{
		if(myAccount[acindex].getAvBalance() < moneyout) {
			this.showMessages("Can't add transaction as you don't have enough moeny");
			return false;
		}
	}
	
	newTransJSON = {
		accountid: myAccount[acindex].getAccountID(),
		date: new Date(transDate),
		comment: transComment,
		amount: transAmount,
		type: transType
	};
	
	this.callAjax("./ajax.php", "addTransAction", JSON.stringify(newTransJSON), this.showMessages);
	
	myAccount[acindex].addTransaction(transDate, transComment, moneyin, moneyout);
	this.newTransCancelBtn();
	this.displayTransactions(acindex);
}

AccountTracker.prototype.newTransCancelBtn = function()
{
	newTransArea = document.getElementById('newTransHook');
	//Destroy form
	while (newTransArea.firstChild)
	{
		newTransArea.removeChild(newTransArea.firstChild);
	}
	// Reenable new button
	document.getElementById('newTransBtn').disabled = false;
}

AccountTracker.prototype.displayTransactions = function(acindex)
{
	//Clear the transaction table first
	this.clearTransTable()
	//Create new transaction table 
	this.makeTransTable();
	//acindex = document.getElementById('accountList').selectedIndex;
	//recalculate the balances and the balance log
	myAccount[acindex].calcBalances();
	notrans = myAccount[acindex].getNoTransactions();
	transTable = document.getElementById('transTable');
	for (i = notrans - 1; i >= 0; i--)
	{
		var listR = document.createElement('TR');
		listR.setAttribute('id', 'listR' + (i));
		var listRC0 = document.createElement('TD');
		listRC0.setAttribute('id', 'List' + (i) + 'RC0');
		var selBox = document.createElement('input');
		selBox.setAttribute('type', 'checkbox');
		selBox.setAttribute('id', 'selBox' + i);
		selBox.setAttribute('name', 'selBox' + i);
		selBox.setAttribute('class', 'selBox');
		selBox.setAttribute('onclick', 'AT.selBoxes()');
		var listRC1 = document.createElement('TD');
		listRC1.setAttribute('id', 'List' + (i) + 'RC1');
		listRC1.setAttribute('class', 'dateCell');
		transdate = new Date(myAccount[acindex].getTransaction(i).getDate());
		dispdate = ""
		if (transdate.getDate() < 10)
		{
			dispdate = "0" + transdate.getDate();
		}
		else
		{
			dispdate = transdate.getDate();
		}
		if (transdate.getMonth() < 9)
		{
			dispdate = dispdate + "/0" + (transdate.getMonth() + 1);
		}
		else
		{
			dispdate = dispdate + "/" + (transdate.getMonth() + 1);
		}
		listRC1.innerHTML = dispdate + "/" + transdate.getFullYear();
		//Number(acBal).toFixed(2)
		var listRC2 = document.createElement('TD');
		listRC2.setAttribute('id', 'List' + (i) + 'RC2');
		listRC2.setAttribute('class', 'commentCell');
		listRC2.innerHTML = myAccount[acindex].getTransaction(i).getComment();
		var listRC3 = document.createElement('TD');
		listRC3.setAttribute('id', 'List' + (i) + 'RC3');
		listRC3.setAttribute('class', 'moneyInCell');
		listRC3.innerHTML = '£' + Number(myAccount[acindex].getTransaction(i).getMoneyIn()).toFixed(2);
		var listRC4 = document.createElement('TD');
		listRC4.setAttribute('id', 'List' + (i) + 'RC4');
		listRC4.setAttribute('class', 'moneyOutCell');
		listRC4.innerHTML = '£' + Number(myAccount[acindex].getTransaction(i).getMoneyOut()).toFixed(2);
		var listRC5 = document.createElement('TD');
		listRC5.setAttribute('id', 'List' + (i) + 'RC5');
		bal = myAccount[acindex].getTransBalance(i);
		if (bal < 0)
		{
			listRC5.setAttribute('class', 'balanceNegCell');
		}
		else
		{
			listRC5.setAttribute('class', 'balancePosCell');
		}
		listRC5.innerHTML = "£" + Number(bal).toFixed(2);
		var listRC6 = document.createElement('TD');
		listRC6.setAttribute('id', 'List' + (i) + 'RC6');
		listRC6.setAttribute('class', 'avBalanceCell');
		listRC6.innerHTML = "£" + Number(Number(Number(myAccount[acindex].getTransBalance(i)) + 
							Number(myAccount[acindex].getOverlimit()))).toFixed(2);
		var listRC7 = document.createElement('TD');
		listRC7.setAttribute('id', 'List' + (i) + 'RC7');
		listRC7.setAttribute('class', 'delCell');
		var delBtn = document.createElement('button');
		delBtn.setAttribute('id', 'delBtn' + i);
		delBtn.setAttribute('onclick', 'AT.delTransBtn(' + i + ')');
		delBtn.innerHTML = "Delete";
		transTable.appendChild(listR);
		listR.appendChild(listRC0);
		listRC0.appendChild(selBox);
		listR.appendChild(listRC1);
		listR.appendChild(listRC2);
		listR.appendChild(listRC3);
		listR.appendChild(listRC4);
		listR.appendChild(listRC5);
		listR.appendChild(listRC6);
		listR.appendChild(listRC7);
		listRC7.appendChild(delBtn);
	}
	//Update our displayed account information
	masterCBalance = document.getElementById('acCBalF');
	masterABalance = document.getElementById('acABalF');
	masterCBalance.innerHTML = "£ " + String(Number(myAccount[acindex].getBalance()).toFixed(2));
	masterABalance.innerHTML = "£ " + String(Number(Number(myAccount[acindex].getBalance()) +
								Number(myAccount[acindex].getOverlimit())).toFixed(2));
}

AccountTracker.prototype.clearTransTable = function()
{
	transListArea = document.getElementById('listTransHook');
	//Destroy form
	while (transListArea.firstChild)
	{
		transListArea.removeChild(transListArea.firstChild);
	}
}

AccountTracker.prototype.makeTransTable = function()
{
	transListArea = document.getElementById('listTransHook');
	var transH = document.createElement('h2');
	transH.setAttribute('id', 'transHead');
	transH.innerHTML = "Transactions"
	var transTable = document.createElement('table')
	transTable.setAttribute('id', 'transTable');
	var listH = document.createElement('THEAD');
	var listHC1 = document.createElement('TH');
	listHC1.innerHTML = "Date";
	var listHC2 = document.createElement('TH');
	listHC2.innerHTML = "Comment";
	var listHC3 = document.createElement('TH');
	listHC3.innerHTML = "Money In";
	var listHC4 = document.createElement('TH');
	listHC4.innerHTML = "Money Out";
	var listHC5 = document.createElement('TH');
	listHC5.innerHTML = "Balance";
	var listHC6 = document.createElement('TH');
	listHC6.innerHTML = "Available <br>Balance";
	var listHC7 = document.createElement('TH');
	listHC7.innerHTML = "Delete";
	var listHC8 = document.createElement('TH');
	var selAllBox = document.createElement('input');
	selAllBox.setAttribute('type', 'checkbox');
	selAllBox.setAttribute('id', 'selectAll');
	selAllBox.setAttribute('name', 'selectAll');
	selAllBox.setAttribute('onclick', 'AT.selAllBoxes()');
	//listHC8.innerHTML = "Select";
	transListArea.appendChild(transH)
	transListArea.appendChild(transTable);
	transTable.appendChild(listH)
	listH.appendChild(listHC8);
	listHC8.appendChild(selAllBox);
	listH.appendChild(listHC1);
	listH.appendChild(listHC2);
	listH.appendChild(listHC3);
	listH.appendChild(listHC4);
	listH.appendChild(listHC5);
	listH.appendChild(listHC6);
	listH.appendChild(listHC7);
}

AccountTracker.prototype.selAllBoxes = function()
{
	selAllBox = document.getElementById('selectAll');
	checkBoxes = document.getElementsByClassName('selBox');
	if (selAllBox.checked)
	{
		boxesOn = true;
		btnOff = false
	}
	else
	{
		boxesOn = false;
		btnOff = true;
	}
	for (i = 0; i < checkBoxes.length; i++)
	{
		checkBoxes[i].checked = boxesOn;
	}
	delTransBtn = document.getElementById('delTransBtn');
	delTransBtn.disabled = btnOff;
}

AccountTracker.prototype.selBoxes = function()
{
	checkBoxes = document.getElementsByClassName('selBox');
	enableBtn = true;
	for (i = 0; i < checkBoxes.length; i++)
	{
		if (checkBoxes[i].checked == true)
		{
			//alert('Checked box found at index: ' + i);
			enableBtn = false;
			break;
		}
	}
	//alert(enableBtn);
	delTransBtn = document.getElementById('delTransBtn');
	delTransBtn.disabled = enableBtn;
}

AccountTracker.prototype.delTransBtn = function(i)
{
	acindex = document.getElementById('accountList').selectedIndex;
	deletedTransaction = myAccount[acindex].delTrans(i);
	delTranJSON = Array();
	delTranJSON[0] = { 
		index: i,
		accountid: deletedTransaction[0].accountid,
		date: deletedTransaction[0].transdate,
		comment: deletedTransaction[0].comment,
		moneyin: deletedTransaction[0].moneyin,
		moneyout: deletedTransaction[0].moneyout
	};
	
	//this.callAjax("./ajax.php", "delTransactions", JSON.stringify(delTranJSON), this.testPHP);
	this.callAjax("./ajax.php", "delTransactions", JSON.stringify(delTranJSON), this.showMessages);
	
	this.displayTransactions(acindex);
}

AccountTracker.prototype.delSelectedTrans = function()
{
	var acindex = document.getElementById('accountList').selectedIndex;
	var itemsToDel = new Array();
	var x = Number(0); // Internal Incremator for items to delete
	var y = Number(0); // Gets the right element of the transaction array
	var checkBoxes = document.getElementsByClassName('selBox');
	// First for loops get the items to be deleted
	for (var i = checkBoxes.length - 1; i >= 0; i--)
	{
		if (checkBoxes[i].checked == true)
		{
			itemsToDel[x] = y;
			x++;
		}
		y++;
	}
	// This loop deletes our items last to first
	delTranJSON = Array();
	delTransID = 0;
	for (i = x - 1; i >= 0; i--)
	{
		deletedTransaction = myAccount[acindex].delTrans(itemsToDel[i]);
		delTranJSON[delTransID] = { 
			index: itemsToDel[i],
			accountid: deletedTransaction[0].accountid,
			date: deletedTransaction[0].transdate,
			comment: deletedTransaction[0].comment,
			moneyin: deletedTransaction[0].moneyin,
			moneyout: deletedTransaction[0].moneyout
		};
		delTransID++;
	}
	
	//alert(JSON.stringify(delTranJSON))
	//this.callAjax("./ajax.php", "delTransactions", JSON.stringify(delTranJSON), this.testPHP);
	this.callAjax("./ajax.php", "delTransactions", JSON.stringify(delTranJSON), this.showMessages);
	this.displayTransactions(acindex);
}

AccountTracker.prototype.reqIcon = function(hook, name)
{
	// Get our source image file
	imgsrc = 'images/req.png';
	// Get our image id... This allow us to reference this later
	imageid = name + "Img";
	// Get a new image object
	var newicon = document.createElement('IMG');
	newicon.setAttribute('src', imgsrc);
	newicon.setAttribute('width', '16');
	newicon.setAttribute('height', '16');
	newicon.setAttribute('id', imageid);
	newicon.setAttribute('class', 'reqIcon');
	newicon.setAttribute('alt', '* Required');
	newicon.setAttribute('style', 'vertical-align: middle;');
	// Attach our icon to the right place on the document
	hook.appendChild(newicon);
}

AccountTracker.prototype.acHelpArea = function()
{
	helpArea = document.getElementById('achelp');
	var helpH = document.createElement('h2');
	helpH.setAttribute('id', 'HelpH');
	helpH.innerHTML = "Help...";
	helpArea.appendChild(helpH);
	var helpP = document.createElement('p');
	helpP.setAttribute('id', 'HelpP');
	helpP.innerHTML = "&nbsp;";
	helpArea.appendChild(helpP);
}

AccountTracker.prototype.acHelp = function(type, hookid)
{
	//Check to see if help is switch off
	if (!this.help)
	{
		return true;
	}
	switch (type)
	{
	case "email":
		helptext = "Please enter a valid e-mail";
		break;
	case "sortcode":
		helptext = "Please enter 3 sets of numbers";
		break;
	case "acno":
		helptext = "Please enter only numbers";
		break;
	case "acname":
		helptext = "Please enter an account name, no HTML";
		break;
	case "money":
		helptext = "Please enter only numbers and a decimal";
		break;
	case "date":
		helptext = "Please enter a date as dd/mm/yyyy e.g. 04/08/2013";
		break;
	case "comment":
		helptext = "Please enter an alpha numeric comment so you can identify the transaction";
		break;
	default:
		helptext = "<br>Opps... We seem to have no help for this item";
	}
	//hook = document.getElementById(hookid).parentNode;
/*
	var helpSpan = document.createElement('span');
	helpSpan.setAttribute('id','HelpSpan');
	helpSpan.setAttribute('class', 'helpText');
	helpSpan.innerHTML = helptext;
	*/
	help = document.getElementById('HelpP');
	help.innerHTML = helptext;
	//help.appendChild(helpSpan);
}

AccountTracker.prototype.acClrHelp = function(hookid)
{
	// Check to see if there is an icon and if there is, is it a cross
	// If there is a cross leave help up
	if (imgHook = document.getElementById(hookid + "Img"))
	{
		// If the feild still has an error don't remove help
		if (imgHook.src == "images/cross.png")
		{
			return false;
		}
	}
	// If help is on chances are help is here so lets clear it.
	if (this.help)
	{
		hook = document.getElementById('HelpP');
		//hook.parentNode.removeChild(hook);
		hook.innerHTML = '&nbsp;';
	}
}

AccountTracker.prototype.acValid = function(edit)
{
	// Set up regular expressions
	//var namere = /^[A-Za-z]+([A-Za-z-\s]+)$/;
	var intre = /^[0-9]([0-9]+)$/;
	var emailre = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var fail = true;
	
	// get our save/new button and turn it off
	if (edit)
	{
		acBtn = document.getElementById('editBtn');
	}
	else
	{
		acBtn = document.getElementById('addBtn');
		//alert("Add Button!");
	}
	
	acBtn.disabled = true;
	// get form items to validate
	acEmail = document.getElementById('Email');
	acSort1 = document.getElementById('code1');
	acSort2 = document.getElementById('code2');
	acSort3 = document.getElementById('code3');
	acNo = document.getElementById('AccountNumber');
	acName = document.getElementById('AccountName');
	if (edit)
	{
		odLimit = document.getElementById('acODLimit');
		acBal = document.getElementById('acBalance');
	}
	
	if (edit)
	{
		// Test in order of form
		if (emailre.test(acEmail.value) && !isNaN(acSort1.value) && !isNaN(acSort2.value) && !isNaN(acSort3.value) && !isNaN(acNo.value) && acName.value.length > 0 && !isNaN(odLimit.value) && !isNaN(acBal.value))
		{
			fail = false;
		}
	}
	else
	{
		// Test in order of form
		//alert("Add Test!")
		
		//alert( "E-Mail Test: " + emailre.test(acEmail.value) + " Sort Test: " + !isNaN(acSort1.value) + !isNaN(acSort2.value) + !isNaN(acSort3.value) +
		//" ACNo Test: " + !isNaN(acNo.value) + " AC Name Length: " + acName.value.length); 
		
		if (emailre.test(acEmail.value) && !isNaN(acSort1.value) && !isNaN(acSort2.value) && !isNaN(acSort3.value) && !isNaN(acNo.value) && acName.value.length > 0)
		{
			fail = false;
		}
	}
	//alert(fail);
	if (!fail)
	{
		acBtn.disabled = false;
		icons = document.getElementsByClassName('reqIcon');
		for (i = 0; i < icons.length; i++)
		{
			icons[i].setAttribute('src', 'images/tick.png');
		}
		//alert(acBtn.name);
		acBtn.disabled = false;
		return true;
	}
	else
	{
		return false;
	}
}

AccountTracker.prototype.acField = function(imgName, edit)
{
	//Get the image we might want to change
	myImg = document.getElementById(imgName);

	//First test the form
	if (!this.acValid(edit))
	{
		myImg.setAttribute('src', 'images/cross.png');
	}
	//Full form validated no point carrying on
	else
	{
		return true;
	}
	
	//Now test the individual part of the form we're using to see if we have the right icon
	if (imgName == 'acEmailImg')
	{
		var emailre = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var email = document.getElementById('Email').value;
		if (emailre.test(email))
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
	}
	//Special handling of the 3 part sortcode feild
	if (imgName == 'sortCodeImg')
	{
		// get the integer test for the sortcode
		var intre = /^[0-9]+([0-9]+)$/;
		// get each of the three fields
		acSort1 = document.getElementById('code1');
		acSort2 = document.getElementById('code2');
		acSort3 = document.getElementById('code3');
		if (!intre.test(acSort1.value))
		{
			acSort1.style.backgroundColor = "red";
		}
		else
		{
			acSort1.style.backgroundColor = "white";
		}
		if (!intre.test(acSort2.value))
		{
			acSort2.style.backgroundColor = "red";
		}
		else
		{
			acSort2.style.backgroundColor = "white";
		}
		if (!intre.test(acSort3.value))
		{
			acSort3.style.backgroundColor = "red";
		}
		else
		{
			acSort3.style.backgroundColor = "white";
		}
		if (intre.test(acSort1.value) && intre.test(acSort2.value) && intre.test(acSort3.value))
		{
			myImg.setAttribute('src', 'images/tick.png');
		}
		else
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
	}
	//Handle the Account number
	if (imgName == "acNoImg")
	{
		var intre = /^[0-9]([0-9]+)$/;
		var acNo = document.getElementById('AccountNumber').value;
		if (intre.test(acNo))
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
	}
	//Handle the Account name
	if (imgName == "acNameImg")
	{
		//var namere = /^[A-Za-z]+([A-Za-z-\s]+)$/;
		var acName = document.getElementById('AccountName').value;
		if (acName.length > 0)
		{
			myImg.setAttribute('src', 'images/tick.png');
			return false;
		}
		else
		{
			myImg.setAttribute('src', 'images/cross.png');
			return false;
		}
	}
}

function startup()
{
	// Get our document hooks
	var actoolbar = document.getElementById("actoolbar");
	var accountobj = document.getElementById("editAccount");
	var transtoolbar = document.getElementById("transtoolbar");
	var transobj = document.getElementById("transactions");
	// Set the AccountTracker Hooks
	if (!AT.SetHooks(actoolbar, accountobj, transtoolbar, transobj))
	{
		alert('AccountTracker failed to get its documents hooks');
		return false;
	}
	AT.loadACToolbar();
	AT.acHelpArea();
	
	//Get data from the server if there is any...
	AT.getServerAccounts();
	
}

AccountTracker.prototype.loadSampleData = function()
{
	accountList = document.getElementById("accountList");
	csvAccounts = document.getElementById('csvSampleAccounts').value;
	//csvTransactions = document.getElementById('csvTransactions');
	preAccounts = csvAccounts.split(':');
	for (i = 0; i < preAccounts.length; i++)
	{
		account = preAccounts[i].split(',');
		code = account[0].split('-');
		myAccount[i] = new Account();
		//alert('Sort Code: ' + code[0] + '-' + code[1] + '-' + code[2]);
		myAccount[i].createAccount(code[0], code[1], code[2], account[1], account[2]);
		if (account[4] == "true")
		{
			od = true;
		}
		else
		{
			od = false;
		}
		myAccount[i].editAccount(code[0], code[1], code[2], account[1], account[2], account[3], od, account[5], account[6])
		accountList.add(new Option(myAccount[i].getAccountName() + 
			" (" + myAccount[i].getSortCode() + " " + 
			myAccount[i].getAccountNo() + ")", i));
		//Get transactions
		csvTransactions = document.getElementById('csvSampleTransAC' + i).value;
		preTransactions = csvTransactions.split(':');
		// Add Transactions to the account
		for (x = 0; x < preTransactions.length; x++)
		{
			//Split the transaction to its parts
			transaction = preTransactions[x].split(',');
			//Convert date to the right format for a date object
			transDateString = transaction[0].split('/');
			var day = Number(transDateString[0]);
			var month = Number(transDateString[1]);
			var year = Number(transDateString[2]);
			var transDate = month + "/" + day + "/" + year
			myAccount[i].addTransaction(transDate, transaction[1], transaction[2], transaction[3]);
		}
	}
	if (accountList.options[0].value == "x")
	{
		accountList.setAttribute('onchange', 'AT.changeAccount()')
		accountList.remove(0);
	}
	
	document.getElementById('ToolBarP').removeChild(document.getElementById('sampleBtn'));
	AT.loadTransToolbar();
	accountList.selectedIndex = 0;
	AT.DisplayAccount(0);
	AT.displayTransactions(0);
	
	//alert(myAccount.length);
	var serverWrite = confirm("Write sample data to server?");
	if (serverWrite == true) {
		for(i = 0; i < myAccount.length; i++) {
			this.callAjax("./ajax.php", "saveFullAccount", JSON.stringify(myAccount[i]), this.accountWritten);
		}
	}
	else {
		this.showMessages("Sample data has not been written to server.");
	}
	
}

AccountTracker.prototype.delAccount = function() {
	accountList = document.getElementById("accountList");
	acIndex = accountList.selectedIndex
	acID = myAccount[acIndex].getAccountID();
	myAccount.splice(acIndex, 1);
		if(myAccount.length > 0){
		accountList.remove(acIndex);
		accountList.selectedIndex = 0;
		AT.DisplayAccount(0);
		AT.displayTransactions(0);
	}
	else {
		accountList.remove(acIndex);
		accountList.add(new Option("No Accounts", "x"));
		this.DestroyForm();
		this.clearTransTable()
		document.getElementById('transToolBarP').parentNode.removeChild(document.getElementById('transToolBarP'));
	}
	this.callAjax("./ajax.php", "delAccount", acID, this.showMessages);
}

// AJAX functions below this point.
AccountTracker.prototype.callAjax = function(url, myFunction, myData, callback)
{
	var xmlhttp;
	// compatible with IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			callback(xmlhttp.responseText);
		}
	}
	xmlhttp.open("POST", url, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	data = "ATActions=" + myFunction + "&myData=" + myData;
	xmlhttp.send(data);
}

AccountTracker.prototype.showMessages = function(message)
{
	messages++
	var msgDIV = document.createElement('DIV');
	msgDIV.setAttribute('id', 'msgDIV' + messages);
	msgDIV.setAttribute('class', 'msgDIV');
	// Build position of DIV
	if(messages == 1) {
		place = "position:absolute; top: 20px; right: 3em;";
	}
	else {
		place = "position:absolute; top: " + ((100 * messages) - 70) + "px; right: 3em;";
	}
	msgDIV.setAttribute("style", place);
	msgDIV.style.opacity = 0;
	var msgH = document.createElement('H1');
	msgH.setAttribute('id', 'msgH' + messages);
	msgH.setAttribute('class', 'msgH');
	msgH.innerHTML = "Server Message";
	var msgIcon = document.createElement('IMG');
	msgIcon.setAttribute('src', 'images/msgIcon.png');
	msgIcon.setAttribute('style', 'float: left; margin-left: 5px; margin-right: 10px;');
	msgIcon.setAttribute('alt', '[message icon]');
	msgIcon.setAttribute('height', '64');
	msgIcon.setAttribute('width', '64');
	var msgP = document.createElement('P');
	msgP.setAttribute('id', 'msgP' + messages);
	msgP.setAttribute('class', 'msgP');
	msgP.innerHTML = message;
	page = document.getElementById('pagecontent');
	msgDIV.appendChild(msgH);
	msgH.appendChild(msgIcon);
	msgDIV.appendChild(msgP);
	page.appendChild(msgDIV);
	// Fade code taken from http://stackoverflow.com/questions/6121203/how-to-do-fade-in-and-fade-out-with-javascript-and-css
	var op = 0.1; // initial opacity
	var timer = setInterval(function()
	{
		if (op >= 1)
		{
			clearInterval(timer);
			//msgDIV.style.display = 'none';
		}
		msgDIV.style.opacity = op;
		msgDIV.style.filter = 'alpha(opacity=' + op * 100 + ")";
		op += op * 0.1;
	}, 50);
	// Enable HTML 5 notification support...
	if (window.webkitNotifications)
	{
		if (window.webkitNotifications.checkPermission() == 0)
		{ // 0 is PERMISSION_ALLOWED
			notification_test = window.webkitNotifications.createNotification('icons/money2.png', 'Account Tracker', message);
			notification_test.show();
		}
		else
		{
			window.webkitNotifications.requestPermission();
		}
	}
	else
	{
		console.log("Notifications are not supported for this Browser/OS version yet.");
	}
	window.setTimeout("AT.hideMessages(" + msgDIV.getAttribute('id') + ");", 5000);
}

AccountTracker.prototype.hideMessages = function(msgDIV)
{
	//alert(msgDIV.id);
	var element = document.getElementById(msgDIV);
	var element = msgDIV;
	var op = 1; // initial opacity
	var timer = setInterval(function()
	{
		if (op <= 0.1)
		{
			clearInterval(timer);
			element.style.display = 'none';
			element.parentNode.removeChild(element);
		}
		element.style.opacity = op;
		element.style.filter = 'alpha(opacity=' + op * 100 + ")";
		op -= op * 0.1;
	}, 50);
	messages--;
}

AccountTracker.prototype.writeAccountToServer = function()
{
	i = document.getElementById("accountList").selectedIndex;
	this.callAjax("./ajax.php", "saveFullAccount", JSON.stringify(myAccount[i]), this.accountWritten);
}

AccountTracker.prototype.accountWritten = function(responseText)
{
	if (responseText == "File Written")
	{
		AT.showMessages("Account successfully written to server.");
	}
	else
	{
		AT.showMessages("There seems to be a problem writing data to the server.");
	}
}

AccountTracker.prototype.getServerAccounts = function()
{
	this.callAjax("./ajax.php", "getACData", "{}", this.showACAccounts);
	//this.callAjax("./ajax.php", "getACData", "{}", this.testPHP);
}

AccountTracker.prototype.testPHP = function(responseText)
{
	//alert(responseText);
	document.write(responseText);
}

AccountTracker.prototype.getDownload = function() {
	i = document.getElementById("accountList").selectedIndex;
	acID = myAccount[i].getAccountID();
	this.callAjax("./ajax.php", "downloadCSV", acID, this.getFile);
	
}

AccountTracker.prototype.getFile = function(responseText) {
	AT.showMessages("File Ready to Download...");
	loc = "getfile.php?filename=" + responseText;
	window.location = loc;
	deleteFileName = responseText;
	window.setTimeout('AT.callAjax("./ajax.php", "unlinkFile", deleteFileName , AT.showMessages)', 7000);
}

AccountTracker.prototype.delFile = function(file) {
	this.callAjax("./ajax.php", "unlinkFile", file, this.showMessages);
}

AccountTracker.prototype.showACAccounts = function(responseText)
{
	if(responseText == "No Files Found") {
		AT.showMessages("No Files Found... Recommended you load the sample data or create your own!")
		return false;
	}
	//alert(responseText);
	newAccounts = JSON.parse(responseText);
	
	//newAccounts = "{}";
	//alert(JSON.stringify(responseText));
	for (i = 0; i < newAccounts.account.length; i++)
	{
		nextAccount = myAccount.length;
		//Test to make sure the account isn't already there...
		match = false;
		if (myAccount.length > 0)
		{
			for (x = 0; x < myAccount.length; x++)
			{
				//alert("MyAccount Index: " + x + " ID: " + myAccount[x].getAccountID());
				if (myAccount[x].getAccountID() == newAccounts.account[i].id)
				{
					match = true;
					break;
				}
			}
		}
		if (!match)
		{
			//Set up a new account
			myAccount[nextAccount] = new Account();
			sort = newAccounts.account[i].sortcode.split('-');
			myAccount[nextAccount].editAccount(
				sort[0], sort[1], sort[2],
				newAccounts.account[i].accountno,
				newAccounts.account[i].accountname,
				newAccounts.account[i].startbalance,
				newAccounts.account[i].overdraft,
				newAccounts.account[i].overlimit,
				newAccounts.account[i].acemail);
			//Add the transactions to the account
			for (x = 0; x < newAccounts.account[i].transactions.length; x++)
			{
				myAccount[nextAccount].addTransaction(
					newAccounts.account[i].transactions[x].transdate,
					newAccounts.account[i].transactions[x].comment,
					newAccounts.account[i].transactions[x].moneyin,
					newAccounts.account[i].transactions[x].moneyout);
			}
			//Add the account to our account list
			accountList = document.getElementById("accountList");
			accountList.add(new Option(myAccount[nextAccount].getAccountName() +
				" (" + myAccount[nextAccount].getSortCode() + " " +
				myAccount[nextAccount].getAccountNo() + ")", i));
		}
	}
	if (!document.getElementById('transToolBarP'))
	{
		AT.loadTransToolbar();
	}
	if (accountList.options[0].value == "x")
	{
		accountList.setAttribute('onchange', 'AT.changeAccount()')
		accountList.remove(0);
	}
	if (document.getElementById('sampleBtn'))
	{
		document.getElementById('sampleBtn').parentNode.removeChild(document.getElementById('sampleBtn'))
	}
	accountList.selectedIndex = 0;
	// Display account
	AT.DisplayAccount(0);
	AT.displayTransactions(0);
	AT.showMessages('Account data successfully loaded from Server');
}
var AT = new AccountTracker();
var myAccount = new Array();
var deleteFileName = "";
var messages = 0;