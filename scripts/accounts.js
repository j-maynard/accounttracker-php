function Account() {
	this.accountid = Number();
	this.sortcode = String();
	this.accountno = Number();
	this.accountname = String();
	this.startbalance = Number();
	this.balance = Number();
	this.overdraft = Boolean();
	this.overlimit = Number();
	this.acemail = String();
	this.transactions = [];
	this.balancelog = [];
}

// Returning functions

Account.prototype.getAccountID = function() {
	return this.accountid;
}

Account.prototype.getSortCode = function() {
	return this.sortcode;
}

Account.prototype.getAccountNo = function() {
	return this.accountno;
}

Account.prototype.getAccountName = function() {
	return this.accountname;
}

Account.prototype.getStartBalance = function() {
	return this.startbalance;
}

Account.prototype.getBalance = function() {
	return this.balance;
}

Account.prototype.getAvBalance = function() {
	if(this.overdraft) {
		avBalance = Number(this.balance) + Number(this.overlimit);
	}
	else {
		avBalance = this.balance
	}
	
	return Number(avBalance);
}

Account.prototype.getOverdraft = function() {
	return this.overdraft;
}

Account.prototype.getOverlimit = function() {
	return this.overlimit;
}

Account.prototype.getNoTransactions = function() {
	return Number(this.transactions.length);
}

Account.prototype.getTransaction = function(transid){
	return this.transactions[transid];
}

Account.prototype.getTransBalance = function(i) {
	return this.balancelog[i].Balance;
}

Account.prototype.getAcEmail = function() {
	return this.acemail;
}

// Setting Functions

Account.prototype.setAccountID = function(sort1,sort2,sort3,acno) {
	this.accountid = "" + sort1 + sort2 + sort3 + acno;
}

Account.prototype.setSortCode = function(sortcode) {
	this.sortcode = sortcode;
}

Account.prototype.setAccountNo = function(accountno) {
	this.accountno = accountno;
}

Account.prototype.setAccountName = function(accountname) {
	this.accountname = accountname;
}

Account.prototype.setBalance = function(balance) {
	this.balance = balance;
}

Account.prototype.setStartBalance = function(balance) {
	this.startbalance = balance;
}

Account.prototype.setOverdraft = function(overdraft) {
	this.overdraft = overdraft;
}

Account.prototype.setOverLimit = function(newlimit) {
	this.overlimit = newlimit;
}

Account.prototype.setAcEmail = function(newemail) {
	this.acemail = newemail;
}

// Construct a new account
Account.prototype.createAccount = function(sort1,sort2,sort3,acno,acname,acemail) {
	this.setAccountID(sort1,sort2,sort3,acno);
	this.sortcode = sort1 + "-" + sort2 + "-" + sort3;
	this.accountno = acno;
	this.accountname = acname;
	this.startbalance = 0.0;
	this.balance = 0.0;
	this.overdraft = false;
	this.overlimit = 0.0;
	this.acemail = acemail;
	
	return true;
}

Account.prototype.editAccount = function(sort1,sort2,sort3,acno,acname,startbal,od,odlimit,acemail) {
	this.setAccountID(sort1,sort2,sort3,acno);
	this.sortcode = sort1 + "-" + sort2 + "-" + sort3;
	this.accountno = acno;
	this.accountname = acname;
	this.startbalance = Number(startbal);
	this.balance = Number(startbal);
	this.overdraft = od;
	this.overlimit = odlimit;
	this.acemail = acemail;
}

Account.prototype.addTransaction = function(datestr, comment, moneyin, moneyout) {
	transid = this.transactions.length;
	this.transactions[transid] = new Transaction();
	this.transactions[transid].createTransaction(transid, this.accountid, datestr, comment, moneyin, moneyout);
}

// Code below taken from: http://stackoverflow.com/questions/1129216/sorting-objects-in-an-array-by-a-field-value-in-javascript
Account.prototype.transSortByDate = function() {
	this.transactions.sort(function compare(a,b) {
		  if (a.transdate < b.transdate)
		     return -1;
		  if (a.transdate > b.transdate)
		    return 1;
		  return 0;
		})
}

Account.prototype.calcBalances = function() {
	balance = this.startbalance
	this.transSortByDate()
	//alert(this.transactions[0].getMoneyIn());
	for(i = 0; i < this.transactions.length; i++) {
		//alert('Creating Accunt Log: ' + i)
		
		moneyin =  this.transactions[i].getMoneyIn();
		moneyout = this.transactions[i].getMoneyOut();
		
		balance = Number(balance) + moneyin - moneyout	;
		avbalance = balance + this.overlimit;
		this.balancelog[i] = {Balance: balance, AVBalance: avbalance};
	}
	
	this.balance = balance;
}

Account.prototype.delTrans = function(i) {
	deletedTransaction = this.transactions.splice(i, 1);
	this.calcBalances();
	return deletedTransaction;
}

