function Transaction() {
	this.accountid;
	//this.transid;
	this.transdate;
	this.moneyin;
	this.moneyout;
	this.comment;
}

// Returning functions

Transaction.prototype.getAccountID = function() {
	return this.accountid;
}

/*
Transaction.prototype.getTransID = function() {
	return this.transid;
}
*/

Transaction.prototype.getMoneyIn = function() {
	return this.moneyin;
}

Transaction.prototype.getMoneyOut = function() {
	return this.moneyout;
}

Transaction.prototype.getComment = function() {
	return this.comment;
}

Transaction.prototype.getDate = function() {
	return this.transdate;
}

// Setting Functions

Transaction.prototype.setAccountID = function(accoundid) {
	this.accountid = accountid;
}

/*
Transaction.prototype.setTransID = function(transid) {
	this.transid = transid;
}
*/

Transaction.prototype.setMoneyIn = function(money) {
	this.mondeyin = money;
}

Transaction.prototype.setMoneyOut = function(money) {
	this.moneyout = money;
}

Transaction.prototype.setComment = function(comment) {
	this.comment = comment;
}

Transaction.prototype.setDate = function(transdate) {
	this.transdate = transdate;
}

Transaction.prototype.createTransaction = function(transid, accountid, datestr, comment, moneyin, moneyout) {
	this.accountid = Number(accountid);
	//this.transid = Number(transid);
	this.transdate = new Date(datestr);
	this.moneyin = Number(moneyin);
	this.moneyout = Number(moneyout);
	this.comment = comment;
}